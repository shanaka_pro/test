<?php


class SimpleJsonRequest
{


    //test

    private static function makeRequest(string $method, string $url, array $parameters = null, array $data = null)
    {
        $opts = [
            'http' => [
                'method'  => $method,
                'header'  => 'Content-type: application/json',
                'content' => $data ? json_encode($data) : null
            ]
        ];

        $url .= ($parameters ? '?' . http_build_query($parameters) : '');


        // set redis
        $redis = new Redis();

        $redis->connect('127.0.0.1', 6379);

        $urls =  $redis->lrange("urls",0,5);

        if (!in_array($url, $urls)) {

            $redis->lpush("urls", $url); 

        } 

        return file_get_contents($url, false, stream_context_create($opts));


    }

    public static function get(string $url, array $parameters = null)
    {

        return self::makeRequest('GET', $url, $parameters);
        // return json_decode(self::makeRequest('GET', $url, $parameters));
    }

    public static function post(string $url, array $parameters = null, array $data)
    {
        return json_decode(self::makeRequest('POST', $url, $parameters, $data));
    }

    public static function put(string $url, array $parameters = null, array $data)
    {
        return json_decode(self::makeRequest('PUT', $url, $parameters, $data));
    }

    public static function patch(string $url, array $parameters = null, array $data)
    {
        return json_decode(self::makeRequest('PATCH', $url, $parameters, $data));
    }

    public static function delete(string $url, array $parameters = null, array $data = null)
    {
        return json_decode(self::makeRequest('DELETE', $url, $parameters, $data));
    }
}


$obj = new SimpleJsonRequest;

$fields = array(
    'target1' => "test",
    'target2' => "test2"
);

echo $obj::get('https://gist.github.com/pxotox/e6f2190685d70f91a2439c9f5b5b482e#file-readme-md', $fields);

